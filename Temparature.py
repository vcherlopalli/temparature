import requests, json

# Enter your API key here
api_key = "f3e70e4c18e796136d23543c1cbbc3e3"

# base_url variable to store url
base_url = "http://api.openweathermap.org/data/2.5/weather?"

# Give city name
city_name = "Delft "

# complete url address
complete_url = base_url + "appid=" + api_key + "&q=" + city_name

# get method of requests module ,return response object
response = requests.get(complete_url)
x= response.json()

if x["cod"] != "404":
        y=x["main"]
        current_temperature = y["temp"]
        print(" Temperature (in Celsius  unit) = " +
                                        str(current_temperature-273.15))
else:
        print(" City Not Found ")
